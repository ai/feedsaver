package feedsaver

import (
	"fmt"
	"io"
	"os"
	"path"
	"strings"
)

type Storage struct {
	Root string
}

func NewStorage(root string) *Storage {
	s := Storage{Root: root}
	return &s
}

func (s *Storage) Store(url string, r io.Reader) error {
	dst_path := s.path_for_url(url)

	// Create the parent directory if needed.
	err := os.MkdirAll(path.Dir(dst_path), 0755)
	if err != nil {
		return err
	}

	// Open the file for writing and dump the request in it.
	f, err := os.Create(dst_path)
	if err != nil {
		return err
	}

	defer f.Close()
	_, err = io.Copy(f, r)
	return err
}

func (s *Storage) Check(url string) bool {
	if _, err := os.Stat(s.path_for_url(url)); err != nil {
		return false
	}
	return true
}

func (s *Storage) path_for_url(url string) string {
	if url[:7] == "http://" {
		url = url[7:]
	} else if url[:8] == "https://" {
		url = url[8:]
	}
	url = strings.TrimRight(url, "/")
	if len(url) > 5 && url[len(url)-5:len(url)] != ".html" {
		url = fmt.Sprintf("%s.html", url)
	}
	return fmt.Sprintf("%s/%s", s.Root, url)
}
