package main

import (
	"flag"
	"autistici.org/feedsaver"
)

var (
	addr = flag.String("addr", ":5000", "TCP address to listen on")
	storage_root = flag.String("storage_dir", "./tmp", "Root of the storage directory")
)

func main() {
	flag.Parse()

	storage := feedsaver.NewStorage(*storage_root)

	feed := feedsaver.NewFeedsaver(storage)
	feed.Run(*addr)
}
