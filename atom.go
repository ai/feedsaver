package feedsaver

import (
	"encoding/xml"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
)

type Feed struct {
	XMLName xml.Name `xml:"http://www.w3.org/2005/Atom feed"`
	Title   string   `xml:"title"`
	Entry   []*Entry `xml:"entry"`
}

type Entry struct {
	Title string `xml:"title"`
	Links []Link `xml:"link"`
}

type Link struct {
	Rel  string `xml:"rel,attr"`
	Href string `xml:"href,attr"`
}

func (f *Feed) Dump() {
	fmt.Printf("[*] %s\n", f.Title)
	for _, t := range f.Entry {
		fmt.Printf("[-] %s\n\t%s\n", t.Links[0].Href, t.Title)
	}
}

func parse_feed(body []byte) (*Feed, error) {
	var feed Feed
	err := xml.Unmarshal(body, &feed)
	if err != nil {
		return nil, err
	}
	return &feed, nil
}

func get_feed(url string) (*Feed, error) {
	log.Printf("downloading Atom feed from %s", url)

	r, err := http.Get(url)
	defer r.Body.Close()
	if err != nil {
		return nil, err
	}

	if r.StatusCode != http.StatusOK {
		return nil, errors.New(
			fmt.Sprintf("http status %d", r.StatusCode))
	}

	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		return nil, err
	}

	return parse_feed(body)
}
